package com.viauc.mod.filemanager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import com.viauc.mod.R;
import com.viauc.mod.tageditor.TagEditor;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Environment;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * FileManager Activity will take care of navigating through the tree
 * of directories and restricts the kind of files allowed to be modified
 * by MP3TagEditor. (which are MP3 files, obviously)
 * 
 * @author Pablo Valencia González
 *
 */

public class FileManager extends Activity {
 
	/** Different kinds of files that FileManager will deal with */
	public static final int ROOT_DIRECTORY = 0;
	public static final int DIRECTORY = 1;
	public static final int MP3_FILE = 2;
	public static final int OTHER = 3;
	public static final int PARENT_DIRECTORY = 4;
	
	/** 
	 * Since the desired way of sorting elements inside a given folder
	 * consist on:
	 * 
	 * 		- 1) Folders, alphabetically sorted.
	 * 		- 2) Files, alphabetically sorted.
	 * 
	 * the choice is to have two separate lists: one for folders and the
	 * other for files. This way, after concatenating them, the result is
	 * the complete list sorted as desired.
	 */
	
	/** Lists to store item names (folders and files) */
	private List<String> _listOfFolders = null;
	private List<String> _listOfFiles = null;
	private List<String> _listOfItems = null;
	
	/** Lists to store item types (folders and files) */
	private List<Integer> _listOfTypesFolders = null;
	private List<Integer> _listOfTypesFiles = null;
	private List<Integer> _listOfTypes = null;
	
	/** Lists to store item paths (folders and files) */
	private List<String> _path = null;
	private List<String> _pathFiles = null;
	private List<String> _pathFolders = null;
	
	/** Path for the root directory */
	private static final String ROOT="/sdcard";
	
	/** TextView that shows the path of the current folder */
	private TextView _pathTextView;
	
	/** ListView that shows the elements inside the current folder */
	private ListView _elementsListView;
	
	private static Context _context;

 

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {
		
		
		
		super.onCreate(savedInstanceState);
		_context = this.getApplicationContext();
			
	    	Log.d("FileManager", "FileManager activity created.");
	    	
	 			setContentView(R.layout.main);
		
	
   	         
   	    	 String extState = Environment.getExternalStorageState();
   	 		if(!extState.equals(Environment.MEDIA_MOUNTED)) {
   	 			
   	 			this.finish();
   	 			Toast.makeText(_context, "SD card not present", Toast.LENGTH_SHORT).show();

   	 		}
   	 		else {

   	 	
   	 	    	getDir(ROOT);
   	 		}

		
    
	}

    
	/**
	 * Fills the ListView with the elements in the folder given by the path
	 * passed as a parameter. Also puts a listener to every one of the elements
	 * so that they perform the right action when pressed.
	 * 
	 * @param dirPath path of the folder with which the ListView is wanted to be filled.
	 */
	private void getDir(String dirPath){
	
		// Initialize the TextView...
		_pathTextView = (TextView)findViewById(R.id.path);
	 	_pathTextView.setText("Location: " + dirPath);
	
	 	// Initialize the lists that contain data..
	  	_listOfFolders = new ArrayList<String>();
	   	_listOfFiles = new ArrayList<String>();
	   	_listOfItems = new ArrayList<String>();
	    	
	   	// Initialize the lists that contain type..
	   	_listOfTypesFolders = new ArrayList<Integer>();
	   	_listOfTypesFiles = new ArrayList<Integer>();
	   	_listOfTypes = new ArrayList<Integer>();
	  	
	   	// Initialize the lists that contain the path for every element...
	   	_path = new ArrayList<String>();
	   	_pathFiles = new ArrayList<String>();
	   	_pathFolders = new ArrayList<String>();
	
	   	// Wrap the folder given by dirPath with a file Object
	   	File f = new File(dirPath);
	   	
	   	// Every screen (except the one at the root directory) will have, 
	   	// at least, a direct link to the root directory and a link to 
	   	// its parent directory.
	   	if(!dirPath.equals(ROOT)){
	
	   		_listOfFolders.add("Root directory");
	   		_listOfTypesFolders.add(FileManager.ROOT_DIRECTORY);
	   		_pathFolders.add(ROOT);
	      
	   		_listOfFolders.add("Parent directory");
	   		_listOfTypesFolders.add(FileManager.PARENT_DIRECTORY);
	   		_pathFolders.add(f.getParent());
	   	}
	
	   	// Retrieve a list of elements in the current path..
	   	File[] files = f.listFiles();	 
	
	   	// And fill the different lists with them...
	   	for(int i=0; i < files.length; i++){
	
	   		File file = files[i];
	   		
	   		// If it's a directory, we give to it its type..
	   		if(file.isDirectory()){
	   			_listOfFolders.add(file.getName());
	   			_listOfTypesFolders.add(FileManager.DIRECTORY);
	   			_pathFolders.add(file.getPath());
	   			Log.d("FileManager.getDir", "Directory added with name " + file.getName() + " and path " + file.getPath());
	   		}else{
	   			
	   			// If it's an MP3 file, a special type is given to the file
	   			if (getExtension(file.getName()).compareTo(".mp3") == 0 ){
	   				_listOfFiles.add(file.getName());
	   				_listOfTypesFiles.add(FileManager.MP3_FILE);
	   				_pathFiles.add(file.getPath());
	   				Log.d("FileManager.getDir", "MP3 file added with name " + file.getName() + " and path " + file.getPath());
	   			}else{
	   				_listOfFiles.add(file.getName());
	   				_listOfTypesFiles.add(FileManager.OTHER);
	   				_pathFiles.add(file.getPath());
	   				Log.d("FileManager.getDir", "Common file added with name " + file.getName() + " and path " + file.getPath());
	   			}
	   		}
	   	}
	    	
	   	// Concatenate data lists...
	   	_listOfItems.addAll(_listOfFolders);
	   	_listOfItems.addAll(_listOfFiles);
	    
	   	// Concatenate type lists...
	   	_listOfTypes.addAll(_listOfTypesFolders);
	   	_listOfTypes.addAll(_listOfTypesFiles);
	    
	   	// Concatenate path lists...
	   	_path.addAll(_pathFolders);
	   	_path.addAll(_pathFiles);
	    	
	   	// Create custom adapter and set it as the adapter for the ListView
	   	ElementAdapter fileList = new ElementAdapter(this, R.layout.row, _listOfItems, _listOfTypes);
	   	_elementsListView = (ListView) this.findViewById(R.id.list);
	   	_elementsListView.setAdapter(fileList);
	
	   	// Set custom OnClickListener for each one of the elements on the ListView
	   	_elementsListView.setOnItemClickListener(new OnItemClickListener() {
	   		
	   		public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
	    	      
	   			File file = new File(_path.get(position));
	
	   	    	if (file.isDirectory()){
	    	    		
	   	    		// If it's a directory and can be read it'll be listed. Otherwise a notification is shown.
	   	    		if(file.canRead()){
	   	    			Log.d("ListView.onItemClick", "The item with name " + _listOfItems.get(position) + " has been clicked and identified as directory.");
	   	    			getDir(_path.get(position));
	   	    		}else{
	   	    			Toast.makeText(_context, "The directory " + file.getName() + " cannot be read", Toast.LENGTH_SHORT).show();
	   	    			Log.d("ListView.onItemClick", "The item with name " + _listOfItems.get(position) + " has been clicked and identified as directory.");
	
	   	    		}
	
	   	    	}else{
	   	    		
	   	    		// if it's an mp3 file a TagEditor class is deployed passing the file's path as an argument.
	   	    		if (getExtension(file.getName()).compareTo(".mp3") == 0 ){
	   	    			
	   	    			Bundle b = new Bundle();
	   	    			b.putString("path", file.getAbsolutePath());
	   	    			Intent i = new Intent();
	   	    			i.setClass(_context, TagEditor.class);
	   	    			i.putExtras(b);
	   	    			startActivity(i);
	   	    			
	   	    		}else{
	   	    			Toast.makeText(_context, "The file " + file.getName() + " is not an MP3 file.", Toast.LENGTH_SHORT).show();
	   	    		}
	   				
	    	    }
	   		} 
	   	});
	}
	
	
	/**
	 * Receives the full name of a given file and returns its extension (if any)
	 * Otherwise, it'll return an empty string.
	 * 
	 * @param name the full name we want to retrieve the extension.
	 * @return a String representing the extension, with a '.' preceding it.
	 */
	private String getExtension(String name) {

		char[] nameArray = name.toCharArray();
		int size = nameArray.length;
		
		if ((nameArray[size-1] == '3') && (nameArray[size-2] == 'p') && (nameArray[size-3] == 'm')) {
			return ".mp3";
		}else{
			return "";
		}
	}
}