package com.viauc.mod.filemanager;

import java.util.List;

import com.viauc.mod.R;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

/**
 * Custom implementation of ArrayAdapter that enables the ListView item's layout redefinition including
 * an icon according to the type of each file.
 * 
 * @author Pablo Valencia González
 */

public class ElementAdapter extends ArrayAdapter<String> {

	private int _rowId;
	private List<Integer> _listOfTypes;


	/**
	 * Creates a newly allocated ElementAdapter object initializing its fields and calling its superclass constructor.
	 */
	public ElementAdapter(FileManager fileManager, int layoutResourceId, List<String> elementList, List<Integer> typesList) {
		
		super(fileManager, layoutResourceId, elementList);
		_rowId = layoutResourceId;
		_listOfTypes = typesList;
	}
	
	
	/**
	 * By overriding this method, which is going to be called for every item in the ListView, the properties of each element
	 * are set as wanted by inflating (parsing) the file row.xml
	 */
	@Override
	public View getView(int position, View convertView, ViewGroup parent){
	
		LinearLayout elementView;
		
		String al = getItem(position);
	    int im = _listOfTypes.get(position);
	 
	    // Inflate the view
	    if(convertView==null){
	       	
	    	elementView = new LinearLayout(getContext());
	    	String inflater = Context.LAYOUT_INFLATER_SERVICE;
	    	LayoutInflater vi = (LayoutInflater)getContext().getSystemService(inflater);
	    	vi.inflate(_rowId, elementView, true);
	       
	    }else{
	    	elementView = (LinearLayout) convertView;
	    }
	       
	    TextView text = (TextView) elementView.findViewById(R.id.textView);	   
	    text.setText(al);
	    
	    // Set the image for every element as it is supposed to be
	    switch(im){
	       
	    case FileManager.ROOT_DIRECTORY:	ImageView imageb = (ImageView) elementView.findViewById(R.id.imageView);
	       									imageb.setImageResource(R.drawable.root_folder);
	       									break;
	          								
	    case FileManager.DIRECTORY:			ImageView imageb2 = (ImageView) elementView.findViewById(R.id.imageView);
	        								imageb2.setImageResource(R.drawable.folder);
	        								break;
	        								
	    case FileManager.PARENT_DIRECTORY:	ImageView imageb5 = (ImageView) elementView.findViewById(R.id.imageView);
											imageb5.setImageResource(R.drawable.arrow);
	        								break;
	        									
	    case FileManager.MP3_FILE:			ImageView imageb3 = (ImageView) elementView.findViewById(R.id.imageView);
	        								imageb3.setImageResource(R.drawable.song);
	        								break;
	        								
	    case FileManager.OTHER:				ImageView imageb4 = (ImageView) elementView.findViewById(R.id.imageView);
	        								imageb4.setImageResource(R.drawable.file);
	        								break;
	    default:
	        								break;
	    }
	              
	    // To avoid problems related to putting focusable elements inside a ListView, this is the easiest and cleanest solution
	    // Discussion about the topic here: http://code.google.com/p/android/issues/detail?id=3414
	    elementView.setDescendantFocusability(LinearLayout.FOCUS_BLOCK_DESCENDANTS);   
	    
	    return elementView;
	    }
}
