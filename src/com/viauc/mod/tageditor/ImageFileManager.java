package com.viauc.mod.tageditor;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Vector;

import org.cmc.music.common.ID3WriteException;
import org.cmc.music.metadata.ImageData;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.metadata.MusicMetadataSet;
import org.cmc.music.myid3.MyID3;

import com.viauc.mod.R;
import android.app.Activity;
import android.app.Dialog;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

/**
 * FileManager Activity will take care of navigating through the tree
 * of directories and restricts the kind of files allowed to be modified
 * by MP3TagEditor. ( which are images )
 * 
 * @author Pablo Valencia González
 *
 */

public class ImageFileManager extends Activity {
 
	/** Different kinds of files that FileManager will deal with */
	public static final int ROOT_DIRECTORY = 0;
	public static final int DIRECTORY = 1;
	public static final int IMAGE = 2;
	public static final int OTHER = 3;
	public static final int PARENT_DIRECTORY = 4;
	
	/** 
	 * Since the desired way of sorting elements inside a given folder
	 * consist on:
	 * 
	 * 		- 1) Folders, alphabetically sorted.
	 * 		- 2) Files, alphabetically sorted.
	 * 
	 * the choice is to have two separate lists: one for folders and the
	 * other for files. This way, after concatenating them, the result is
	 * the complete list sorted as desired.
	 */
	
	/** Lists to store item names (folders and files) */
	private List<String> _listOfFolders = null;
	private List<String> _listOfFiles = null;
	private List<String> _listOfItems = null;
	
	/** Lists to store item types (folders and files) */
	private List<Integer> _listOfTypesFolders = null;
	private List<Integer> _listOfTypesFiles = null;
	private List<Integer> _listOfTypes = null;
	
	/** Lists to store item paths (folders and files) */
	private List<String> _path = null;
	private List<String> _pathFiles = null;
	private List<String> _pathFolders = null;
	
	/** Path for the root directory */
	private static final String ROOT="/sdcard";
	
	/** TextView that shows the path of the current folder */
	private TextView _pathTextView;
	
	/** ListView that shows the elements inside the current folder */
	private ListView _elementsListView;
	
	/** This link to the current activity will be used as the context in
	 *  situations which it's not accessible via this or getApplicationContext()*/
	private Activity _activity;
	
	/** Path of the image that'll be used as the cover image */
	private String _imagePath;
	
	/** The path of the song whose image is wanted to be updated */
	private String _songPath;
	
 

	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.main);
    	Log.d("FileManager", "FileManager activity created.");
        
    	_songPath = this.getIntent().getExtras().getString("path");
    	_activity = this;
        
    	_pathTextView = (TextView)findViewById(R.id.path);
    	getDir(ROOT);
    
	}

    
	/**
	 * Fills the ListView with the elements in the folder given by the path
	 * passed as a parameter. Also puts a listener to every one of the elements
	 * so that they perform the right action when pressed.
	 * 
	 * @param dirPath path of the folder with which the ListView is wanted to be filled.
	 */
	private void getDir(String dirPath){
	
		// Initialize the TextView...
	 	_pathTextView.setText("Location: " + dirPath);
	
	 	// Initialize the lists that contain data..
	  	_listOfFolders = new ArrayList<String>();
	   	_listOfFiles = new ArrayList<String>();
	   	_listOfItems = new ArrayList<String>();
	    	
	   	// Initialize the lists that contain type..
	   	_listOfTypesFolders = new ArrayList<Integer>();
	   	_listOfTypesFiles = new ArrayList<Integer>();
	   	_listOfTypes = new ArrayList<Integer>();
	  	
	   	// Initialize the lists that contain the path for every element...
	   	_path = new ArrayList<String>();
	   	_pathFiles = new ArrayList<String>();
	   	_pathFolders = new ArrayList<String>();
	
	   	// Wrap the folder given by dirPath with a file Object
	   	File f = new File(dirPath);
	   	
	   	// Every screen (except the one at the root directory) will have, 
	   	// at least, a direct link to the root directory and a link to 
	   	// its parent directory.
	   	if(!dirPath.equals(ROOT)){
	
	   		_listOfFolders.add("Root directory");
	   		_listOfTypesFolders.add(ImageFileManager.ROOT_DIRECTORY);
	   		_pathFolders.add(ROOT);
	      
	   		_listOfFolders.add("Parent directory");
	   		_listOfTypesFolders.add(ImageFileManager.PARENT_DIRECTORY);
	   		_pathFolders.add(f.getParent());
	   	}
	
	   	// Retrieve a list of elements in the current path..
	   	File[] files = f.listFiles();	 
	
	   	// And fill the different lists with them...
	   	for(int i=0; i < files.length; i++){
	
	   		File file = files[i];
	   		
	   		// If it's a directory, we give to it its type..
	   		if(file.isDirectory()){
	   			_listOfFolders.add(file.getName());
	   			_listOfTypesFolders.add(ImageFileManager.DIRECTORY);
	   			_pathFolders.add(file.getPath());
	   			Log.d("FileManager.getDir", "Directory added with name " + file.getName() + " and path " + file.getPath());
	   		}else{
	   			
	   			// If it's an MP3 file, a special type is given to the file
	   			if (getExtension(file.getName()).compareTo(".jpg") == 0 ){
	   				_listOfFiles.add(file.getName());
	   				_listOfTypesFiles.add(ImageFileManager.IMAGE);
	   				_pathFiles.add(file.getPath());
	   				Log.d("FileManager.getDir", "MP3 file added with name " + file.getName() + " and path " + file.getPath());
	   			}else if (getExtension(file.getName()).compareTo(".jpg") == 0 ){
	   				_listOfFiles.add(file.getName());
	   				_listOfTypesFiles.add(ImageFileManager.IMAGE);
	   				_pathFiles.add(file.getPath());
	   				Log.d("FileManager.getDir", "MP3 file added with name " + file.getName() + " and path " + file.getPath());
	   			}else{
	   				_listOfFiles.add(file.getName());
	   				_listOfTypesFiles.add(ImageFileManager.OTHER);
	   				_pathFiles.add(file.getPath());
	   				Log.d("FileManager.getDir", "Common file added with name " + file.getName() + " and path " + file.getPath());
	   			}
	   		}
	   	}
	    	
	   	// Concatenate data lists...
	   	_listOfItems.addAll(_listOfFolders);
	   	_listOfItems.addAll(_listOfFiles);
	    
	   	// Concatenate type lists...
	   	_listOfTypes.addAll(_listOfTypesFolders);
	   	_listOfTypes.addAll(_listOfTypesFiles);
	    
	   	// Concatenate path lists...
	   	_path.addAll(_pathFolders);
	   	_path.addAll(_pathFiles);
	    	
	   	// Create custom adapter and set it as the adapter for the ListView
	   	ImageElementAdapter fileList = new ImageElementAdapter(this, R.layout.row, _listOfItems, _listOfTypes);
	   	_elementsListView = (ListView) this.findViewById(R.id.list);
	   	_elementsListView.setAdapter(fileList);
	
	   	// Set custom OnClickListener for each one of the elements on the ListView
	   	_elementsListView.setOnItemClickListener(new OnItemClickListener() {
	   		
	   		public void onItemClick(AdapterView<?> adapter, View view, int position, long arg) {
	    	      
	   			File file = new File(_path.get(position));
	   			
	   			_imagePath = _path.get(position);
	   			
	   	    	if (file.isDirectory()){
	    	    		
	   	    		// If it's a directory and can be read it'll be listed. Otherwise a notification is shown.
	   	    		if(file.canRead()){
	   	    			Log.d("ListView.onItemClick", "The item with name " + _listOfItems.get(position) + " has been clicked and identified as directory.");
	   	    			getDir(_path.get(position));
	   	    		}else{
	   	    			Toast.makeText(_activity.getApplicationContext(), "The directory " + file.getName() + " cannot be read", Toast.LENGTH_SHORT).show();
	   	    			Log.d("ListView.onItemClick", "The item with name " + _listOfItems.get(position) + " has been clicked and identified as directory.");
	
	   	    		}
	
	   	    	}else{
	   	    		
	   	    		// if it's a jpg image, it'll be updated properly
	   	    		if (getExtension(file.getName()).compareTo(".jpg") == 0 ){
	   	    		
	   	    			// preliminar dialog which'll show the image we want to include
	   	    			final Dialog dialog = new Dialog(_activity);

	   	    			dialog.setContentView(R.layout.customalertmessage);
	   	    			dialog.setTitle("Selected image");

	   	    			ImageView image = (ImageView) dialog.findViewById(R.id.imageView1);
	   	    			image.setImageDrawable(Drawable.createFromPath(file.getAbsolutePath()));
	   	    			
	   	    			dialog.show();
	   	    			
	   	    			// give a meaning to the "Cancel" button...
	   	    			Button notUseThisImageButton = (Button) dialog.findViewById(R.id.button2);
	   	    			notUseThisImageButton.setOnClickListener(new View.OnClickListener() {
							
							public void onClick(View v) {
								
								dialog.dismiss();
							}
						});
	   	    			
	   	    			// give a meaning to the "Use this image" button..
	   	    			Button useThisImageButton = (Button) dialog.findViewById(R.id.button1);
	   	    			useThisImageButton.setOnClickListener(new View.OnClickListener() {
	   	    				
	   	    			  public void onClick(View v) {
				
	   	    				  
	   	    				ProgressDialog spinnerDialog = ProgressDialog.show(  
	   	 	    			    _activity, "",  
	   	 	    			   "Loading image...", true);  
	   	 	    			new Thread(new Runnable() {  
	   	 	    			    public void run() {  
	   	 	    			    	
		   	 	    			    try {
		
				   	    				
				   	    				
				   	    				// Convert the image taken from the file to an array of
				   	    				// byte, in order to add it to the ID3 tag
				   	    				Drawable d = Drawable.createFromPath(_imagePath);
				   	    				Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
				   	    				ByteArrayOutputStream stream = new ByteArrayOutputStream();
				   	    				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
				   	    				byte[] bitmapdata = stream.toByteArray();
				   	    			    
				   	    			    File f = new File(_songPath);
				   	    			    
				   	    			    MyID3 _id3Tag = new MyID3();
				   	    			    MusicMetadataSet _set = _id3Tag.read(f);
				   	    			    MusicMetadata mm = _set.id3v2Raw.values;
				   	    			    
				   	    			    Vector<ImageData> vector= new Vector<ImageData>();
				   	    			    vector.add(0, new ImageData(bitmapdata,"image/jpeg","",0));
				   	    			    mm.setPictureList(vector);
				   	    		
				   	    				_id3Tag.update(f, _set, mm);
				   	    				
				   	    				dialog.dismiss();
				   	    				_activity.finish();
				   	    				
				   	    				// Once the image is updated, a new TagEditor activity will be deployed
				   	    				// in order to see the changes.
				   	    				Bundle b = new Bundle();
				   	    				b.putString("path", _songPath);
				   	 	    			Intent i = new Intent();
				   	 	    			i.setClass(_activity, TagEditor.class);
				   	 	    			i.putExtras(b);
				   	 	    			startActivity(i);
				   	    		
				   	    			    
				   	    			} catch (IOException e) {
				   	    				
				   	    				e.printStackTrace();
				   	    			} catch (ID3WriteException e) {
										// TODO Auto-generated catch block
										e.printStackTrace();
									}
			   	    			  }
	
		   	 	    			}).start(); 
	
	   	    			  }});
	   	    			
	   	    		// if it's a jpeg image, it'll be updated properly	
	   	    		}else if (getExtension(file.getName()).compareTo(".jpeg") == 0 ){	
	   	    			
	   	    			// preliminar dialog which'll show the image we want to include
	   	    			final Dialog dialog = new Dialog(_activity);

	   	    			dialog.setContentView(R.layout.customalertmessage);
	   	    			dialog.setTitle("Selected image");

	   	    			ImageView image = (ImageView) dialog.findViewById(R.id.imageView1);
	   	    			image.setImageDrawable(Drawable.createFromPath(file.getAbsolutePath()));
	   	    			
	   	    			dialog.show();
	   	    			
	   	    			// give a meaning to the "Cancel" button...
	   	    			Button notUseThisImageButton = (Button) dialog.findViewById(R.id.button2);
	   	    			notUseThisImageButton.setOnClickListener(new View.OnClickListener() {
							
							public void onClick(View v) {
								
								dialog.dismiss();
							}
						});
	   	    			
	   	    			// give a meaning to the "Use this image" button..
	   	    			Button useThisImageButton = (Button) dialog.findViewById(R.id.button1);
	   	    			useThisImageButton.setOnClickListener(new View.OnClickListener() {
	   	    				
	   	    			  public void onClick(View v) {
				
	   	    				ProgressDialog spinnerDialog = ProgressDialog.show(  
		   	 	    			    _activity, "",  
		   	 	    			   "Loading image...", true);  
		   	 	    			new Thread(new Runnable() {  
		   	 	    			    public void run() {  
		   	 	    			    	
			   	 	    			    try {
			
					   	    		
					   	    				
					   	    				// Convert the image taken from the file to an array of
					   	    				// byte, in order to add it to the ID3 tag
					   	    				Drawable d = Drawable.createFromPath(_imagePath);
					   	    				Bitmap bitmap = ((BitmapDrawable)d).getBitmap();
					   	    				ByteArrayOutputStream stream = new ByteArrayOutputStream();
					   	    				bitmap.compress(Bitmap.CompressFormat.JPEG, 100, stream);
					   	    				byte[] bitmapdata = stream.toByteArray();
					   	    			    
					   	    			    File f = new File(_songPath);
					   	    			    
					   	    			    MyID3 _id3Tag = new MyID3();
					   	    			    MusicMetadataSet _set = _id3Tag.read(f);
					   	    			    MusicMetadata mm = _set.id3v2Raw.values;
					   	    			    
					   	    			    Vector<ImageData> vector= new Vector<ImageData>();
					   	    			    vector.add(0, new ImageData(bitmapdata,"image/jpeg","",0));
					   	    			    mm.setPictureList(vector);
					   	    		
					   	    				_id3Tag.update(f, _set, mm);
					   	    				
					   	    				dialog.dismiss();
					   	    				_activity.finish();
					   	    				
					   	    				
					   	    				// Once the image is updated, a new TagEditor activity will be deployed
					   	    				// in order to see the changes.
					   	    				Bundle b = new Bundle();
					   	    				b.putString("path", _songPath);
					   	 	    			Intent i = new Intent();
					   	 	    			i.setClass(_activity, TagEditor.class);
					   	 	    			i.putExtras(b);
					   	 	    			startActivity(i);
					   	    		
					   	    			    
					   	    			} catch (IOException e) {
					   	    				
					   	    				e.printStackTrace();
					   	    			} catch (ID3WriteException e) {
											// TODO Auto-generated catch block
											e.printStackTrace();
										}
				   	    			  }
		
			   	 	    			}).start(); 
		
		   	    			  }});
	   	    			
	   	    		}else{
	   	    			
	   	    			Toast.makeText(_activity.getApplicationContext(), "The file " + file.getName() + " is not an image.", Toast.LENGTH_SHORT).show();
	   	    		}
	    	    }
	   		} 
	   	});
	}
	
	
	/**
	 * Receives the full name of a given file and returns its extension (if any)
	 * Otherwise, it'll return an empty string.
	 * 
	 * @param name the full name we want to retrieve the extension.
	 * @return a String representing the extension, with a '.' preceding it.
	 */
	private String getExtension(String name) {

		char[] nameArray = name.toCharArray();
		int size = nameArray.length;
		
		if ((nameArray[size-1] == 'g') && (nameArray[size-2] == 'p') && (nameArray[size-3] == 'j')) {
			return ".jpg";
		}else if ((nameArray[size-1] == 'g') &&  (nameArray[size-2] == 'e') && (nameArray[size-3] == 'p') && (nameArray[size-4] == 'j')){
			return ".jpeg";
		}else{
			return "";
		}
	}
}
