package com.viauc.mod.tageditor;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.Vector;

import org.cmc.music.common.ID3WriteException;
import org.cmc.music.metadata.ImageData;
import org.cmc.music.metadata.MusicMetadata;
import org.cmc.music.metadata.MusicMetadataSet;
import org.cmc.music.myid3.MyID3;

import com.viauc.mod.R;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Spinner;
import android.widget.TextView;
import android.widget.Toast;
 
/**
 * TagEditor Activity will provide a simple interface to modify the 
 * metadata of the file passed as a parameter. It allows to modify the
 * name of the song, the artist of the song, the name of the album,
 * the music genre, the year and the cover picture.
 * 
 * @author Pablo Valencia González
 *
 */
public class TagEditor extends Activity{
	
	/** It'll extract the metadata from the created file*/
	private MyID3 _id3Tag;
	
	/** Represents the metadata extracted from the file and provides a
	  * way to choose which interface is wanted to be used.*/
	private MusicMetadataSet _set;
	
	/** Concrete interface of the ID3 tag that'll be used to modify/consult
	  * the metadata.*/
	MusicMetadata _mm;
	
	/** Full path of the file whose metadata is wanted to be changed/consulted */
	private String _filePath;
	
	/** Wraps the _filePath, providing an interface to read/write from the file*/
	private File _file;
	
	/** Since the music styles are more or less known, the genre is going to be
	 *  consulted/updated by using an spinner */
	private Spinner _spinnerGenre;
	
	/** This reference to the current activity is going to be used as the context
	  * for the different elements that need a context and it's not accessible by
	  * saying this or getApplicationContext().*/
	private Activity _activity;
	
	/** Called when the activity is first created. */
	@Override
	public void onCreate(Bundle savedInstanceState) {

		super.onCreate(savedInstanceState);
		setContentView(R.layout.tageditor);
		_activity = this;
		
		// populate the spinner
		_spinnerGenre = (Spinner) findViewById( R.id.spinnerGenre );
		ArrayAdapter<CharSequence> adapter = ArrayAdapter.createFromResource(
				  							 getApplicationContext(), R.array.array_Genres, android.R.layout.simple_spinner_item );
											 adapter.setDropDownViewResource( android.R.layout.simple_spinner_dropdown_item );
		_spinnerGenre.setAdapter( adapter );
		
		
		// and fill the new window with the song's metadata.
		try {
			
			_filePath = this.getIntent().getExtras().getString("path");
			_file = new File(_filePath);
			_id3Tag = new MyID3();
			_set = _id3Tag.read(_file);
			fillLayout(_set);
			
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		// give a meaning to the button underlying the cover image
		ImageButton imageButton = (ImageButton) findViewById(R.id.imageButtonSong);
		imageButton.setOnClickListener(new OnClickListener() {
 
			public void onClick(View arg0) {
				
				
	    		
	    		ProgressDialog spinnerDialog = ProgressDialog.show(  
	    			    _activity, "",  
	    			   "Please wait...", true);  
	    			new Thread(new Runnable() {  
	    			    public void run() {  
	    			    	
	    			    	// Deploy an ImageFileManager to select a new cover image
	    					Bundle b = new Bundle();
	    		    		b.putString("path", _file.getAbsolutePath());
	    		    		Intent i = new Intent();
	    		    		i.setClass(_activity.getApplicationContext(), ImageFileManager.class);
	    		    		i.putExtras(b);
	    			    	
	    			    	// The current Activity will be finished and recreated to show the new
	    		    		// cover image. And because of this, in case of previous modifications
	    		    		// the rest of fields will be saved.
	    		    		_activity.finish();
	    		    		startActivity(i);
	    		    		saveStuff();
	    			    }  
	    			}).start(); 
	    		
	    		
	    		
			}

			/**
			 *  Saves the information of all the fields except the cover image.
			 */
			private void saveStuff() {
				
				EditText textName = (EditText) _activity.findViewById(R.id.editTextName);
				_mm.setSongTitle(textName.getText().toString());
				EditText textArtist = (EditText) _activity.findViewById(R.id.editTextArtist);
				_mm.setArtist(textArtist.getText().toString());
				EditText textAlbum = (EditText) _activity.findViewById(R.id.editTextAlbum);
				_mm.setAlbum(textAlbum.getText().toString());
				EditText textYear = (EditText) _activity.findViewById(R.id.editTextYear);
				_mm.setYear(textYear.getText().toString());
				
				_mm.setGenre(_spinnerGenre.getSelectedItem().toString());

				try {
					_id3Tag.update(_file, _set, _mm);
				} catch (UnsupportedEncodingException e) {
					e.printStackTrace();
				} catch (ID3WriteException e) {
					e.printStackTrace();
				} catch (IOException e) {
					e.printStackTrace();
				}		
			}
		});	
	}

	/**
	 * Fills the new window with the song's metadata.
	 * 
	 * @param musicMetadataSet Represents the metadata extracted from the file and provides a
	 * way to choose which interface is wanted to be used.
	 * 
	 * @throws IOException If any problem happens with I/O tasks.
	 */
	private void fillLayout(MusicMetadataSet musicMetadataSet) throws IOException {
	
		// Choice: Raw interface and id3v2 type of TAG
		_mm = musicMetadataSet.id3v2Raw.values;
		
		// Fill the textViews...
		TextView fileName = (TextView) this.findViewById(R.id.fileNameTextView);
		fileName.setText(new File(_filePath).getName());
		EditText textName = (EditText) this.findViewById(R.id.editTextName);
		textName.setText(_mm.getSongTitle());
		EditText textArtist = (EditText) this.findViewById(R.id.editTextArtist);
		textArtist.setText(_mm.getArtist());
		EditText textAlbum = (EditText) this.findViewById(R.id.editTextAlbum);
		textAlbum.setText(_mm.getAlbum());
		EditText textYear = (EditText) this.findViewById(R.id.editTextYear);
		textYear.setText(_mm.getYear());

		// For the SpinnerGenre: if the genre matches any of the predefined
		// genres, it's set. Otherwise the default choice will be "Unclassifiable"
		if (_mm.getGenre().compareTo("Alternative") == 0 ){
			_spinnerGenre.setSelection(0);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Books") == 0 ){
			_spinnerGenre.setSelection(1);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Blues") == 0 ){
			_spinnerGenre.setSelection(2);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Children’s Music") == 0 ){
			_spinnerGenre.setSelection(3);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Classical") == 0 ){
			_spinnerGenre.setSelection(4);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Country") == 0 ){
			_spinnerGenre.setSelection(5);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Dance") == 0 ){
			_spinnerGenre.setSelection(6);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Data") == 0 ){
			_spinnerGenre.setSelection(7);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Easy Listening") == 0 ){
			_spinnerGenre.setSelection(8);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Electronica") == 0 ){
			_spinnerGenre.setSelection(9);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Folk") == 0 ){
			_spinnerGenre.setSelection(10);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Gospel") == 0 ){
			_spinnerGenre.setSelection(11);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Hip Hop") == 0 ){
			_spinnerGenre.setSelection(12);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Holiday") == 0 ){
			_spinnerGenre.setSelection(13);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Industrial") == 0 ){
			_spinnerGenre.setSelection(14);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Jazz") == 0 ){
			_spinnerGenre.setSelection(15);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Latin") == 0 ){
			_spinnerGenre.setSelection(16);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Metal") == 0 ){
			_spinnerGenre.setSelection(17);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("New Age") == 0 ){
			_spinnerGenre.setSelection(18);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Pop") == 0 ){
			_spinnerGenre.setSelection(19);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Punk") == 0 ){
			_spinnerGenre.setSelection(20);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Rap") == 0 ){
			_spinnerGenre.setSelection(21);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Reggae") == 0 ){
			_spinnerGenre.setSelection(22);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Religious") == 0 ){
			_spinnerGenre.setSelection(23);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("R&B") == 0 ){
			_spinnerGenre.setSelection(24);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Rock") == 0 ){
			_spinnerGenre.setSelection(25);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Soundtrack") == 0 ){
			_spinnerGenre.setSelection(26);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("Spoken") == 0 ){
			_spinnerGenre.setSelection(27);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else if (_mm.getGenre().compareTo("World") == 0) {
			_spinnerGenre.setSelection(29);
			Log.i("FUA","element selected: " + _mm.getGenre());
		}else{
			_spinnerGenre.setSelection(28);
		}
		
		// populate the imageButtonSong...
		ImageView imageView = (ImageView) this.findViewById(R.id.imageButtonSong);
		@SuppressWarnings("unchecked")
		Vector<ImageData> _imageVector = _mm.getPictureList();
		
		if ( _imageVector.size() > 0 ){ 
			
			// if there's a picture to show, the program shows it 
			InputStream in = new ByteArrayInputStream(_imageVector.get(0).imageData);
			Bitmap bMap = BitmapFactory.decodeStream(in);
			in.close();
			imageView.setImageBitmap(bMap);
			
		} else {
			
			// otherwise we show a default picture
			imageView.setImageResource(R.drawable.no_cover);
		}
	}
	
	/**
	 * Action performed when the "Save changes" button is pressed.
	 */
	public void onSaveClick(View view){  
		
		ProgressDialog spinnerDialog = ProgressDialog.show(  
			    _activity, "",  
			   "Saving changes...", true);  
			new Thread(new Runnable() {  
			    public void run() {  
			    	
			    	// retrieve the data from the fields...
					EditText textName = (EditText) _activity.findViewById(R.id.editTextName);
					_mm.setSongTitle(textName.getText().toString());
					EditText textArtist = (EditText) _activity.findViewById(R.id.editTextArtist);
					_mm.setArtist(textArtist.getText().toString());
					EditText textAlbum = (EditText) _activity.findViewById(R.id.editTextAlbum);
					_mm.setAlbum(textAlbum.getText().toString());
					EditText textYear = (EditText) _activity.findViewById(R.id.editTextYear);
					_mm.setYear(textYear.getText().toString());
					_mm.setGenre(_spinnerGenre.getSelectedItem().toString());

					// and update the ID3 tag
					try {
						_id3Tag.update(_file, _set, _mm);
					} catch (UnsupportedEncodingException e) {
						e.printStackTrace();
					} catch (ID3WriteException e) {
						e.printStackTrace();
					} catch (IOException e) {
						e.printStackTrace();
					}
					
					_activity.finish();
			    }  
			}).start(); 
		
		
		
		
		
	}  
	
	/**
	 * Action performed when the "Cancel" button is pressed.
	 */
	public void onCancelClick(View view) throws IOException{  
		
		this.finish();
	}
	
	/**
	 * Action performed when the "magic button" is pressed.
	 */
	public void onMagicClick(View view){  
		
		/** NOTE: Even if the library is correctly implemented, the way of interfacing with the
		 * server has changed recently, so it always returns Error 404. That's why it is disabled.
		 */
		
		/*
		MusicBrainz server = new MusicBrainzImpl();
		
		//name of an album
		String name = "A";
		//medium query
		int depth = 4;
		//maximum number of items to return
		int maxItems = 1;

		Model results;

		try {
			results = server.findArtistByName(name, depth, maxItems);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		*/
		
		Toast.makeText(getApplicationContext(), "Metadata retrieval disabled.", Toast.LENGTH_SHORT).show();
		
	}
	

}
